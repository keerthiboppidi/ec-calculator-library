import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Calculator from "./components";
import { GlobalProvider } from "./context/GlobalState";

import  "./styles/mainstyles.scss";

const App = () => {
  return (
    <div>
      <GlobalProvider>
        <Router>
          <Switch>
            <Route exact path="/DecimalFractionTable/:id?" component={Calculator.DecimalFractionTable} />
            <Route exact path="/DBMToWattsConversionCalculator/:id?" component={Calculator.DBMToWattsConversionCalculator} />
          </Switch>
        </Router>
      </GlobalProvider>
    </div>
  )
}

export default App
